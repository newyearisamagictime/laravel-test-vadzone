Тут все как в обычном html <br/>

<?php // начало php кода

// это однострочный комментарий, он начинается с двойного слеша
// чтобы поставить его в шторме, нажми ctrl+/

/*
    это многострочный комменарий
    чтобы поставить его в шторме, выдели текст и нажми ctrl+shift+/
 */

$variable = 1; // это переменная

/*
    переменная обозначается как доллар + некое название
    допускается наименование переменных на кириллице, но никогда так не делай
    и без транслита - лучше плохой перевод, чем что-то вроде $kolicestvo_tovarov
    название переменной может содержать буквы, цифры и знак подчеркивания
    при этом с цифры начинаться не может
    если переменная состоит из несольких слов, делай так $firstSecondThird
 */

/*
    переменная содержит некое значение
    это может быть:
    0. null
    1. строка
    2. число
    3. массив
    4. объект
    5. функция
    6. true / false
    мб, что-то еще
*/

$variableNull = null;
$variableString1 = 'строка';
$variableString2 = "строка в двойных кавычках";
$variableNumber1 = 666;
$variableNumber2 = 666.666; // для отделения дробной части используй точку
$variableArray = [1, 2, 3]; // простой массив - неупорядоченный список
$variableAssocArray = [
    'keyNumber' => 666,
    'keyString' => 'строка',
    'keySubArray' => [/* содержимое вложенного массива */],
]; // ассоциативный массив - список, у каждного из членов которого есть ключ

/*
    объекты и функции рассмотрим потом
*/

$variableBooleanTrue = true; // истина
$variableBooleanFalse = false; // ложь

// и че с этими переменными делать?

// 1) самое простое - вывести на экран
echo $variableString2; // выведет "строка в двойных кавычках" (только без кавычек)
print($variableString2);

// 2) создавать новые переменные, например, складывая существующие
$variableNumber3 = $variableNumber1 + $variableNumber2; // станет 1332.666
// 3) или добавляя к старым просто любые числа
$variableNumber4 = $variableNumber1 + 1000; // станет 1666
// 4) точно так же можно переопределять значения старых переменных
$variableNumber1 = $variableNumber2; // станет 666.666
// то же самое с вычитанием, умножением и делением

// 5) можно складывать строки через точку (это зовется конкатенация)
$variableString3 = $variableString1 . $variableString2; // будет "строкастрока в двойных кавычках"
// или так
$variableString3 = $variableString1 . ' хуй'; // будет "строка хуй"
// 6) можно создавать новую строку, встраивая переменные в строку в ДВОЙНЫХ кавычках
$variableString4 = "слово $variableString3 $variableString1"; // будет "слово строка хуй строка"
$variableString4 = 'слово ' . $variableString3 . ' ' . $variableString1; // будет "слово строка хуй строка"

// УСЛОВИЯ

if ($variableBooleanTrue == true) {
    echo 'variableBooleanTrue равно истине';
}

if ($variableBooleanFalse == true) {
    echo 'variableBooleanFalse равно истине, что неверно';
} else {
    echo 'variableBooleanFalse равно лжи';
}

if ($variableBooleanFalse == 1) {
    echo 'variableBooleanFalse равно 1, что какой-то пиздец';
} elseif ($variableBooleanFalse == true) {
    echo 'variableBooleanFalse равно истине, что неверно';
} else {
    echo 'variableBooleanFalse равно лжи';
}

$number = 1;
if ($number == 4) {
    //
} elseif ($number >= 10) {
    //
} elseif ($number == 40) {
    //
} else {
    //
}

// РАБОТА С МАССИВАМИ
$simpleArray = [1, 2, 3];
// чтоб получить значение элемента массива, нужно обратиться к массиву с указанием индекса или ключа
// у простого массива нет ключей - только индексы
// считаются от 0
echo $simpleArray[0]; // это 1
echo $simpleArray[1]; // это 2
echo $simpleArray[2]; // это 3
echo $simpleArray[3]; // это нихуя

$assocArray = [
    'type' => 'человек',
    'name' => 'Вадзон',
    'length' => '20 сантиметров',
];
// ассоциативный массив имеет ключи
echo $assocArray['type']; // это "человек"
echo $assocArray['name']; // это "Вадзон"
echo $assocArray['length']; // это недостижимая мечта, лол

// многомерный массив
$multiArray = [
    'mike' => [
        'type' => 'человек',
        'name' => 'Мике',
        'length' => '20 сантиметров',
    ],
    'dim' => [
        'type' => 'человек',
        'name' => 'Вадзон',
        'length' => '20 сантиметров',
    ],
];
echo $multiArray['mike']['type'];
echo $multiArray['dim']['name'];

// ФУНКЦИИ

function functionName() { // название функции по правилам названия переменной
    // какой-то код
}

functionName();

/*
    например, есть код, который ты постоянно копипастишь
    к примеру, этот код создает строку типа <label><input type="text">Надпись</label>
*/

$result = "<label>";
$result = $result . "<input type='text'>";
$result = $result . "Надпись";
$result = $result . "</label>";

// то есть ты постоянно добавляешь в конец строки $result новую инфу
// при чем, ты можешь постоянно менять надпись и тип инпута
// то есть, иногда ты юзаешь код, который написан сверху, а иногда что-то вроде

$result = "<label>";
$result = $result . "<input type='number'>";
$result = $result . "Другая надпись";
$result = $result . "</label>";

// в итоге у тебя таких конструкций развелось дохуя
// и в какой-то момент ты понял, что тебе теперь в каждый кусок нужно добавить класс для лабеля
// типа так

$result = "<label class='new-class'>";
$result = $result . "<input type='number'>";
$result = $result . "Другая надпись";
$result = $result . "</label>";

// ты расстраиваешься и решаешь написать функцию

function createLabel($title, $inputType){
    $res = "<label class='$inputType'>";
    $res = $res . "<input type='number'>";
    $res = $res . $title;
    $res = $res . "</label>";

    return $res;
}

// и используешь так

$result = createLabel('Надпись', 'number');

// ниже - конец php кода
// если предполагается, что в файле есть только php код, то ставить его не обязательно
?>
