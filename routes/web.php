<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/vadzon', function () {
    return 'vodzon';
});

Route::get('/new-vadzon', function () {
    return view('new-vadzon');
});

Route::get('/page/{id}', function ($id) {
    if ($id == 1) {
        $pageTitle = 'Page 1';
        $pageText = 'Text 1';
    } elseif ($id == 2) {
        $pageTitle = 'Page 2';
        $pageText = 'Text 2';
    } else {
        $pageTitle = 'Idi naxui';
        $pageText = 'Text dlya togo kto poidet hanui';
    }

    return view('page', [
        'pageTitle' => $pageTitle,
        'pageText' => $pageText,
    ]);
});
